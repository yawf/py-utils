import setuptools


setuptools.setup(
    name='yawf-py-utils',
    version='0.2.0',
    author='Maksim Penkov',
    author_email='me@madmax.im',
    description='Various reusable Python utilities (HTTP Exceptions, aiohttp middlewares, e.t.c.)',
    url='https://gitlab.com/yawf/py-utils',
    packages=setuptools.find_namespace_packages(
        include=('yawf.*'),
        exclude=('tests', 'tests.*',),
    ),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
