import logging

import aiohttp.web


_logger = logging.getLogger(__name__)


UNKNOWN_ERROR = 'UNKNOWN_ERROR'


class AppHTTPException(Exception):
    def __init__(self, status, code, description=None):
        super().__init__('Exception status={}, code={}'.format(status, code))

        try:
            if issubclass(status, aiohttp.web.HTTPError):
                status = status.status_code
        except TypeError:
            pass

        self.status = status
        self.code = code
        self.description = description

    @staticmethod
    def from_client_error(client_err: aiohttp.web.HTTPClientError):
        code = aiohttp.http.RESPONSES.get(
            client_err.status, (UNKNOWN_ERROR, None)
        )[0]
        code = code.replace(' ', '_')
        code = code.upper()

        return AppHTTPException(
            status=client_err.status,
            code=code,
            description=str(client_err),
        )


@aiohttp.web.middleware
async def json_exception_middleware(request, handler):
    try:
        return await handler(request)
    except aiohttp.web.HTTPClientError as ex:
        return _response(AppHTTPException.from_client_error(ex))
    except AppHTTPException as ex:
        return _response(ex)
    except BaseException as ex:
        _logger.exception('Unexpected exception', exc_info=ex)
        return _response(AppHTTPException(
            status=aiohttp.web.HTTPInternalServerError,
            code=UNKNOWN_ERROR,
        ))


def _json_data(ex: AppHTTPException):
    exception_data = {
        'code': ex.code,
    }

    if ex.description:
        exception_data['description'] = ex.description

    return exception_data


def _response(ex: AppHTTPException):
    return aiohttp.web.json_response(
        data=_json_data(ex),
        status=ex.status,
    )
