import json
import os

import pytest


@pytest.fixture
def locate_file(request):
    test_path = request.fspath
    test_directory = os.path.dirname(test_path)

    def locator(filename):
        for module in _py_modules(test_directory):
            file_path = os.path.join(
                module,
                'data',
                filename,
            )

            if os.path.exists(file_path):
                return file_path

        raise ValueError('Unable to find ' + filename)

    return locator


@pytest.fixture
def load_data(locate_file):
    def reader(fp):
        return fp.read()

    def loader(filename):
        return _data_loader(locate_file(filename), 'rb', reader)

    return loader


@pytest.fixture
def load_json(locate_file):
    def loader(filename):
        return _data_loader(locate_file(filename), 'r', json.load)

    return loader


def _data_loader(filename, mode, loader):
    with open(filename, mode) as fp:
        return loader(fp)


def _is_py_module(directory):
    return os.path.exists(
        os.path.join(
            directory,
            '__init__.py',
        )
    )


def _py_modules(directory):
    while _is_py_module(directory):
        yield directory
        directory = os.path.dirname(directory)
